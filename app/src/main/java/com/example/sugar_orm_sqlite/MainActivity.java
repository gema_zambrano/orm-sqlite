package com.example.sugar_orm_sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editTextitulo,editTextedicion,editTexID;
    Button buttonguardar,buttonmodificar,buttoneliminar,buttonConsultaIndividual,buttonConsultaGeneral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editTextitulo=findViewById(R.id.editTextitulo);
        editTextedicion=findViewById(R.id.editTextedicion);
        editTexID=findViewById(R.id.editTexID);
        //botones
        buttonguardar=findViewById(R.id.buttonguardar);
        buttonmodificar=findViewById(R.id.buttonmodificar);
        buttoneliminar=findViewById(R.id.buttoneliminar);

        buttonConsultaGeneral=findViewById(R.id.buttonConsultaGeneral);
        buttonConsultaIndividual=findViewById(R.id.buttonConsultaIndividual);


        // guardar
        buttonguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book registro1=new Book(editTextitulo.getText().toString(),editTextedicion.getText().toString());
                registro1.save();
                Log.e("guardar","datos guardados");

            }
        });
        buttonmodificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = Book.findById(Book.class, Long.parseLong(editTexID.getText().toString()));
                book.title= editTextitulo.getText().toString();
                book.edition=editTextedicion.getText().toString();
                book.save();
            }
        });

        buttoneliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Book book = Book.findById(Book.class,Long.parseLong(editTexID.getText().toString()));
                book.delete();
                Log.e("eliminar","dato eliminado");

            }
        });
        // consulta individual
        buttonConsultaIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book=Book.findById(Book.class,Long.parseLong(editTexID.getText().toString()));
                editTextedicion.setText(book.getEdition());
                editTextitulo.setText(book.getTitle());
            }
        });




// datos estaticos para guardar
       // Book registro1= new Book("HARRY POTTER","PRIMERA");
        //registro1.save();
        //Log.e("guardar","guardar datos!");
    }
}
